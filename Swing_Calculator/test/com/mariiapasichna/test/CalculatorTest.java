package com.mariiapasichna.test;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.mariiapasichna.models.Calculator;
import com.mariiapasichna.models.Result;

class CalculatorTest {
	Calculator calculator = new Calculator();

	@Test
	void testDefault() {
		assertEquals(new Result(4), calculator.executeStrategy(2, 2));
	}

	@Test
	void testAddition() {
		calculator.setStrategy("+");
		assertEquals(new Result(2 + 2), calculator.executeStrategy(2, 2));
		assertEquals(new Result(-2 + 2), calculator.executeStrategy(-2, 2));
		assertEquals(new Result(0.0 + 2), calculator.executeStrategy(0.0, 2));
	}

	@Test
	void testSubtraction() {
		calculator.setStrategy("-");
		assertEquals(new Result(2 - 2), calculator.executeStrategy(2, 2));
		assertEquals(new Result(-2 - 22), calculator.executeStrategy(-2, 22));
		assertEquals(new Result(0.0 - 0.0), calculator.executeStrategy(0.0, 0.0));
	}

	@Test
	void testMultiplication() {
		calculator.setStrategy("*");
		assertEquals(new Result(2 * 2), calculator.executeStrategy(2, 2));
		assertEquals(new Result(-2 * -256), calculator.executeStrategy(-2, -256));
		assertEquals(new Result(2.0 * 0.0), calculator.executeStrategy(2.0, 0.0));
	}

	@Test
	void testDivision() {
		calculator.setStrategy("/");
		assertEquals(new Result(2 / 2), calculator.executeStrategy(2, 2));
		assertEquals(new Result(-562 / 2), calculator.executeStrategy(-562, 2));
		assertEquals(new Result(-2.0 / -782), calculator.executeStrategy(-2.0, -782));
	}

	@Test
	void testDivisionByZero() {
		calculator.setStrategy("/");
		assertEquals(new Result(2 / 0.0), calculator.executeStrategy(2, 0.0));
		assertEquals("Infinity", calculator.executeStrategy(2, 0).getResult() + "");
		assertEquals("NaN", calculator.executeStrategy(0.0, 0.0).getResult() + "");
		assertEquals(new Result(0.0 / 0.0), calculator.executeStrategy(0.0, 0.0));
	}

	@Test
	void testThrows() {
		assertThrows(IllegalArgumentException.class, () -> calculator.setStrategy("$"));
	}
}