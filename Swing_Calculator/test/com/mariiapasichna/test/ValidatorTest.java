package com.mariiapasichna.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.mariiapasichna.utils.Validator;

class ValidatorTest {

	@Test
	void testValidateTextField() {
		assertEquals(true, Validator.validateTextField("123"));
		assertEquals(true, Validator.validateTextField("-123"));
		assertEquals(true, Validator.validateTextField("123."));
		assertEquals(true, Validator.validateTextField("123.0"));
		assertEquals(true, Validator.validateTextField("0.0"));
		assertEquals(true, Validator.validateTextField("0"));
		assertEquals(false, Validator.validateTextField(".0"));
		assertEquals(false, Validator.validateTextField("--123"));
		assertEquals(false, Validator.validateTextField("12-3"));
		assertEquals(false, Validator.validateTextField("123 "));
		assertEquals(false, Validator.validateTextField(" "));
		assertEquals(false, Validator.validateTextField(""));
		assertEquals(false, Validator.validateTextField("qwer"));
		assertEquals(false, Validator.validateTextField("123,"));
	}
}