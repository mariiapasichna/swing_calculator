package com.mariiapasichna.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

import com.mariiapasichna.models.Result;

class ResultTest {

	@Test
	void testMessage() {
		Result result = new Result("TestMessage");
		assertEquals("TestMessage", result.getMessage());
		result.setMessage("Test");
		assertEquals("Test", result.getMessage());
		assertNotNull(result.getMessage());
		assertEquals(0.0, result.getResult());
		assertEquals(new Result("Test"), result);
	}

	@Test
	void testResult() {
		Result result = new Result(56);
		assertEquals(56, result.getResult());
		result.setResult(56.0);
		assertEquals(56.0, result.getResult());
		assertNull(result.getMessage());
	}
}