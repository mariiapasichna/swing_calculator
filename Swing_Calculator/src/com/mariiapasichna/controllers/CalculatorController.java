package com.mariiapasichna.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import com.mariiapasichna.models.Calculator;
import com.mariiapasichna.models.Result;
import com.mariiapasichna.utils.Validator;
import com.mariiapasichna.views.TabbedPane;

/**
 * The {@code CalculatorController} class responsible to store data in Result
 * object and update view TabbedPane accordingly. CalculatorController acts on
 * both model and view. It controls the data flow into model object and updates
 * the view whenever data changes. It keeps view and model separate.
 * 
 * @author Mariia Pasichna
 */
public class CalculatorController {
	private TabbedPane tabbedPane;
	private Calculator calculator;

	/**
	 * Create an {@code CalculatorController} object holding the values of the
	 * specified {@code TabbedPane} and {@code Calculator}.
	 * 
	 * @param calculator the value of type Calculator to be stored in variable
	 *                   calculator
	 * @param tabbedPane the value of type TabbedPane to be stored in variable
	 *                   tabbedPane
	 */
	public CalculatorController(Calculator calculator, TabbedPane tabbedPane) {
		this.tabbedPane = tabbedPane;
		this.calculator = calculator;
		addViewListeners();
	}

	private void addViewListeners() {
		addTextField1Listener();
		addTextField2Listener();
		addComboBoxListener();
		addButtonListener();
		addCheckBoxListener();
	}

	private void addTextField1Listener() {
		JTextField textField = tabbedPane.getPanelCalculator().getTextField1();

		textField.addCaretListener(new CaretListener() {

			@Override
			public void caretUpdate(CaretEvent e) {

				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						validate(textField);
					}
				});
			}
		});
	}

	private void addTextField2Listener() {
		JTextField textField = tabbedPane.getPanelCalculator().getTextField2();

		textField.addCaretListener(new CaretListener() {

			@Override
			public void caretUpdate(CaretEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						validate(textField);
					}
				});
			}
		});
	}

	private void addComboBoxListener() {
		JComboBox<String> comboBox = tabbedPane.getPanelCalculator().getComboBox();
		comboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				calculator.setStrategy(comboBox.getItemAt(comboBox.getSelectedIndex()));
			}
		});
	}

	private void addButtonListener() {
		JButton button = tabbedPane.getPanelCalculator().getButton();
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (button.getActionCommand().equals("Calculate")) {
					calculate();
				}
			}
		});
	}

	private void addCheckBoxListener() {
		JCheckBox checkBox = tabbedPane.getPanelCalculator().getCheckBox();
		JButton button = tabbedPane.getPanelCalculator().getButton();
		checkBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (checkBox.isSelected()) {
					button.setEnabled(false);
				} else {
					button.setEnabled(true);
				}
			}
		});
	}

	private void calculate() {
		Result result = calculator.executeStrategy(
				Double.valueOf(tabbedPane.getPanelCalculator().getTextField1().getText()),
				Double.valueOf(tabbedPane.getPanelCalculator().getTextField2().getText()));
		String output;
		if (result.getMessage() != null) {
			output = result.getMessage();
		} else {
			output = "" + result.getResult();
		}
		tabbedPane.getPanelCalculator().getTextFieldResult().setText(output);
		tabbedPane.getPanelHistory().getResults().add(0, output);
	}

	private void validate(JTextField textField) {
		if (tabbedPane.getPanelCalculator().getCheckBox().isSelected()) {
			if (Validator.validateTextField(textField.getText())) {
				calculate();
			} else {
				textField.setText("0.0");
				calculate();
			}
		} else if (!Validator.validateTextField(textField.getText())) {
			textField.setText("0.0");
		}
	}
}