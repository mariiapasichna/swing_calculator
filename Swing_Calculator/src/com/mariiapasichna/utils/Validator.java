package com.mariiapasichna.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The {@code Validator} class provides static method to validate text fields
 * according to constant TEXT_FIELD_PATTERN.
 * 
 * @author Mariia Pasichna
 */
public class Validator {
	private static final String TEXT_FIELD_PATTERN = "[-]?\\d+(\\.\\d*){0,}";
	private static Pattern pattern;
	private static Matcher matcher;

	/**
	 * Validate text field according to pattern.
	 * 
	 * @param input String value of field to be validated
	 * @return {@code true} if, and only if, the entire region sequence matches this
	 *         matcher's pattern
	 */
	public static boolean validateTextField(String input) {
		pattern = Pattern.compile(TEXT_FIELD_PATTERN);
		matcher = pattern.matcher(input);
		return matcher.matches();
	}
}