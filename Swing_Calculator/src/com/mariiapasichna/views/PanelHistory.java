package com.mariiapasichna.views;

import java.awt.BorderLayout;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;


/**
 * Instances of this class represent a selectable user interface object that
 * represent a page in a notebook widget. It is a generic lightweight container.
 * The {@code PanelHistory} class extends JPanel.
 * 
 * @author Mariia Pasichna
 *
 */
public class PanelHistory extends JPanel {
	private DefaultListModel<String> results;
	private JList<String> list;
	private JScrollPane listScrollPane;

	/**
	 * Constructs a new instance of this class and describe its behavior and appearance.
	 */
	public PanelHistory() {
		setLayout(new BorderLayout());
		results = new DefaultListModel<>();
		list = new JList<>(results);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listScrollPane = new JScrollPane(list);
		add(listScrollPane, BorderLayout.CENTER);
	}

	/**
	 * Return the value stored in field results.
	 * 
	 * @return results the DefaultListModel<String> value stored in variable results
	 */
	public DefaultListModel<String> getResults() {
		return results;
	}

	/**
	 * Set the value into field results.
	 * 
	 * @param results the DefaultListModel<String> value to be stored in variable results
	 */
	public void setResults(DefaultListModel<String> results) {
		this.results = results;
	}

	/**
	 * Return the value stored in field list.
	 * 
	 * @return list the JList<String> value stored in variable list
	 */
	public JList<String> getList() {
		return list;
	}

	/**
	 * Set the value into field list.
	 * 
	 * @param list the JList<String> value to be stored in variable list
	 */
	public void setList(JList<String> list) {
		this.list = list;
	}

	/**
	 * Return the value stored in field listScrollPane.
	 * 
	 * @return listScrollPane the JScrollPane value stored in variable listScrollPane
	 */
	public JScrollPane getListScrollPane() {
		return listScrollPane;
	}

	/**
	 * Set the value into field listScrollPane.
	 * 
	 * @param listScrollPane the JScrollPane value to be stored in variable listScrollPane
	 */
	public void setListScrollPane(JScrollPane listScrollPane) {
		this.listScrollPane = listScrollPane;
	}
}