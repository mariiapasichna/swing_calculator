package com.mariiapasichna.views;

import javax.swing.JTabbedPane;

/**
 * A component that lets the user switch between a group of components by
 * clicking on a tab with a given title and/or icon. The {@code TabbedPane}
 * class extends JTabbedPane.
 * 
 * @author Mariia Pasichna
 *
 */
public class TabbedPane extends JTabbedPane {
	private PanelCalculator panelCalculator;
	private PanelHistory panelHistory;

	/**
	 * Creates TabbedPane with two children with a default tab placement of
	 * JTabbedPane.TOP.
	 */
	public TabbedPane() {
		panelCalculator = new PanelCalculator();
		panelHistory = new PanelHistory();
		add("Calculator", panelCalculator);
		add("History", panelHistory);
	}

	/**
	 * Return the value stored in field panelCalculator.
	 * 
	 * @return panelCalculator the PanelCalculator value stored in variable
	 *         panelCalculator
	 */
	public PanelCalculator getPanelCalculator() {
		return panelCalculator;
	}

	/**
	 * Set the value into field panelCalculator.
	 * 
	 * @param panelCalculator the PanelCalculator value to be stored in variable
	 *                        panelCalculator
	 */
	public void setPanelCalculator(PanelCalculator panelCalculator) {
		this.panelCalculator = panelCalculator;
	}

	/**
	 * Return the value stored in field panelHistory.
	 * 
	 * @return panelHistory the PanelHistory value stored in variable panelHistory
	 */
	public PanelHistory getPanelHistory() {
		return panelHistory;
	}

	/**
	 * Set the value into field panelHistory.
	 * 
	 * @param panelHistory the PanelHistory value to be stored in variable
	 *                     panelHistory
	 */
	public void setPanelHistory(PanelHistory panelHistory) {
		this.panelHistory = panelHistory;
	}
}