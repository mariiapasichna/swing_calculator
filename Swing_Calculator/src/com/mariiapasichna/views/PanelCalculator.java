package com.mariiapasichna.views;

import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

/**
 * Instances of this class represent a selectable user interface object that
 * represent a page in a notebook widget. It is a generic lightweight container.
 * The {@code PanelHistory} class extends JPanel.
 * 
 * @author Mariia Pasichna
 *
 */
public class PanelCalculator extends JPanel {
	private JTextField textField1;
	private JTextField textField2;
	private JTextField textFieldResult;
	private JCheckBox checkBox;
	private JButton button;
	private JLabel label;
	private JComboBox<String> comboBox;

	/**
	 * Constructs a new instance of this class and describe its behavior and appearance.
	 */
	public PanelCalculator() {
		setLayout(new BorderLayout());

		textField1 = new JTextField("0.0");
		textField2 = new JTextField("0.0");
		textFieldResult = new JTextField();
		textFieldResult.setHorizontalAlignment(JTextField.RIGHT);

		String[] operations = { "+", "-", "/", "*" };
		comboBox = new JComboBox<>(operations);

		button = new JButton("Calculate");

		checkBox = new JCheckBox("Calculate on the fly");

		label = new JLabel("Result:");

		Border empty = BorderFactory.createEmptyBorder(10, 10, 10, 10);

		JPanel panelInput = new JPanel();
		panelInput.setLayout(new BoxLayout(panelInput, BoxLayout.LINE_AXIS));
		panelInput.add(textField1);
		panelInput.add(Box.createHorizontalGlue());
		panelInput.add(comboBox);
		panelInput.add(Box.createHorizontalGlue());
		panelInput.add(textField2);
		panelInput.setBorder(empty);

		JPanel panelCalculate = new JPanel();
		panelCalculate.setLayout(new BoxLayout(panelCalculate, BoxLayout.LINE_AXIS));
		panelCalculate.add(checkBox);
		panelCalculate.add(Box.createHorizontalGlue());
		panelCalculate.add(button);
		panelCalculate.setBorder(empty);

		JPanel panelResult = new JPanel();
		panelResult.setLayout(new BoxLayout(panelResult, BoxLayout.LINE_AXIS));
		panelResult.add(label);
		panelResult.add(Box.createHorizontalGlue());
		panelResult.add(textFieldResult);
		panelResult.setBorder(empty);

		add(panelInput, BorderLayout.NORTH);
		add(panelCalculate, BorderLayout.CENTER);
		add(panelResult, BorderLayout.SOUTH);
	}

	/**
	 * Return the value stored in field textField1.
	 * 
	 * @return textField1 the JTextField value stored in variable textField1
	 */
	public JTextField getTextField1() {
		return textField1;
	}

	/**
	 * Set the value into field textField1.
	 * 
	 * @param textField1 the JTextField value to be stored in variable textField1
	 */
	public void setTextField1(JTextField textField1) {
		this.textField1 = textField1;
	}

	/**
	 * Return the value stored in field textField2.
	 * 
	 * @return textField2 the JTextField value stored in variable textField2
	 */
	public JTextField getTextField2() {
		return textField2;
	}

	/**
	 * Set the value into field textField2.
	 * 
	 * @param textField2 the JTextField value to be stored in variable textField2
	 */
	public void setTextField2(JTextField textField2) {
		this.textField2 = textField2;
	}

	/**
	 * Return the value stored in field textFieldResult.
	 * 
	 * @return textFieldResult the JTextField value stored in variable textFieldResult
	 */
	public JTextField getTextFieldResult() {
		return textFieldResult;
	}

	/**
	 * Set the value into field textFieldResult.
	 * 
	 * @param textFieldResult the JTextField value to be stored in variable textFieldResult
	 */
	public void setTextFieldResult(JTextField textFieldResult) {
		this.textFieldResult = textFieldResult;
	}

	/**
	 * Return the value stored in field checkBox.
	 * 
	 * @return checkBox the JCheckBox value stored in variable checkBox
	 */
	public JCheckBox getCheckBox() {
		return checkBox;
	}

	/**
	 * Set the value into field checkBox.
	 * 
	 * @param checkBox the JCheckBox value to be stored in variable checkBox
	 */
	public void setCheckBox(JCheckBox checkBox) {
		this.checkBox = checkBox;
	}

	/**
	 * Return the value stored in field button.
	 * 
	 * @return button the JButton value stored in variable button
	 */
	public JButton getButton() {
		return button;
	}

	/**
	 * Set the value into field button.
	 * 
	 * @param button the JButton value to be stored in variable button
	 */
	public void setButton(JButton button) {
		this.button = button;
	}

	/**
	 * Return the value stored in field label.
	 * 
	 * @return label the JLabel value stored in variable label
	 */
	public JLabel getLabel() {
		return label;
	}

	/**
	 * Set the value into field label.
	 * 
	 * @param label the JLabel value to be stored in variable label
	 */
	public void setLabel(JLabel label) {
		this.label = label;
	}

	/**
	 * Return the value stored in field comboBox.
	 * 
	 * @return comboBox the JComboBox<String> value stored in variable comboBox
	 */
	public JComboBox<String> getComboBox() {
		return comboBox;
	}

	/**
	 * Set the value into field comboBox.
	 * 
	 * @param comboBox the JComboBox<String> value to be stored in variable comboBox
	 */
	public void setComboBox(JComboBox<String> comboBox) {
		this.comboBox = comboBox;
	}
}