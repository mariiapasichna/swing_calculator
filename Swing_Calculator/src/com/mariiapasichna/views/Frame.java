package com.mariiapasichna.views;

import java.awt.BorderLayout;
import javax.swing.JFrame;

/**
 * Instances of this class build the behavior of JFrame class, representation of
 * the "windows" which the desktop or "window manager" is managing. The
 * {@code Frame} class extends JFrame.
 * 
 * @author Mariia Pasichna
 *
 */
public class Frame extends JFrame {

	/**
	 * Constructs a new instance of this class that is initially invisible.
	 * 
	 * @param tabbedPane child object
	 */
	public Frame(TabbedPane tabbedPane) {
		setTitle("Swing Calculator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(350, 370);
		setLocationRelativeTo(null);
		setLayout(new BorderLayout());
		add(tabbedPane);
		setVisible(true);
	}
}