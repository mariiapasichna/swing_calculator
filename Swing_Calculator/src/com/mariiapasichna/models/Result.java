package com.mariiapasichna.models;

import java.util.Objects;

/**
 * The object {@code Result} class acting as a model, wraps a value of the
 * primitive type {@code double} and object type {@code String} in an object. An
 * object of type {@code Result} contains two fields type {@code double} and
 * {@code String}.
 * 
 * @author Mariia Pasichna
 *
 */
public class Result {
	private double result;
	private String message;

	/**
	 * Create an {@code Result} object holding the value of the specified
	 * {@code double}.
	 *
	 * @param result the value to be stored in variable result
	 */
	public Result(double result) {
		this.result = result;
	}

	/**
	 * Create an {@code Result} object holding the value of the specified
	 * {@code String}.
	 *
	 * @param message the value to be stored in variable message
	 */
	public Result(String message) {
		this.message = message;
	}

	/**
	 * Return the value stored in field message.
	 * 
	 * @return message the String value stored in variable message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Set the value into field message.
	 * 
	 * @param message the String value to be stored in variable message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Return the value stored in field result.
	 * 
	 * @return result the double value stored in variable result
	 */
	public double getResult() {
		return result;
	}

	/**
	 * Set the value into field result.
	 * 
	 * @param result the double value to be stored in variable result
	 */
	public void setResult(double result) {
		this.result = result;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Result))
			return false;
		Result result1 = (Result) o;
		return Double.compare(result1.getResult(), getResult()) == 0
				&& Objects.equals(getMessage(), result1.getMessage());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getResult(), getMessage());
	}
}