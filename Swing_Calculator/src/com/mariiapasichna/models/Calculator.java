package com.mariiapasichna.models;

import com.mariiapasichna.strategies.AdditionStrategy;
import com.mariiapasichna.strategies.CalculationStrategy;
import com.mariiapasichna.strategies.DivisionStrategy;
import com.mariiapasichna.strategies.MultiplicationStrategy;
import com.mariiapasichna.strategies.SubtractionStrategy;


/**
 * The {@code Calculator} class provides methods to set and execute strategy. An
 * object of type {@code Calculator} contains the single field type
 * {@code Strategy}.
 * 
 * @author Mariia Pasichna
 */
public class Calculator {
	private CalculationStrategy strategy;

	/**
	 * Create an {@code Calculator} object holding the value of the specified
	 * {@code Strategy} with default value AdditionStrategy.
	 */
	public Calculator() {
		this.strategy = new AdditionStrategy();
	}

	/**
	 * Set the value into field strategy.
	 * 
	 * @param operation String representation of operation
	 * @throws IllegalArgumentException if operation has unexpected value
	 */
	public void setStrategy(String operation) {
		switch (operation) {
		case "+":
			this.strategy = new AdditionStrategy();
			break;
		case "-":
			this.strategy = new SubtractionStrategy();
			break;
		case "*":
			this.strategy = new MultiplicationStrategy();
			break;
		case "/":
			this.strategy = new DivisionStrategy();
			break;
		default:
			throw new IllegalArgumentException("Unexpected value: " + operation);
		}
	}

	/**
	 * Execute operation according to strategy
	 * 
	 * @param a expression operand
	 * @param b expression operand
	 * @return the object of type Result
	 */
	public Result executeStrategy(double a, double b) {
		return strategy.calculate(a, b);
	}
}