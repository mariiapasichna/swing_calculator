package com.mariiapasichna;

import javax.swing.SwingUtilities;

import com.mariiapasichna.controllers.CalculatorController;
import com.mariiapasichna.models.Calculator;
import com.mariiapasichna.views.Frame;
import com.mariiapasichna.views.TabbedPane;

public class CalculatorApp {
	public static void main(String[] args) {
		TabbedPane tabbedPane = new TabbedPane();
		Frame frame = new Frame(tabbedPane);
		SwingUtilities.invokeLater(() -> new CalculatorController(new Calculator(), tabbedPane));
	}
}