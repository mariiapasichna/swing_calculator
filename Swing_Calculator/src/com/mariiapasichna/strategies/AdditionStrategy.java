package com.mariiapasichna.strategies;

import com.mariiapasichna.models.Result;

/**
 * The {@code AdditionStrategy} class implements interface
 * {@code CalculationStrategy} and sets own behavior according to the strategy
 * pattern.
 * 
 * @author Mariia Pasichna
 *
 */
public class AdditionStrategy implements CalculationStrategy {

	/**
	 * Execute operation addition according to AdditionStrategy
	 * 
	 * @param a expression operand of double type
	 * @param b expression operand of double type
	 * @return the object of type Result
	 */
	@Override
	public Result calculate(double a, double b) {
		return new Result(a + b);
	}
}