package com.mariiapasichna.strategies;

import com.mariiapasichna.models.Result;

/**
 * The {@code DivisionStrategy} class implements interface
 * {@code CalculationStrategy} and sets own behavior according to the strategy
 * pattern.
 * 
 * @author Mariia Pasichna
 *
 */
public class DivisionStrategy implements CalculationStrategy {
	
	/**
	 * Execute operation division according to DivisionStrategy
	 * 
	 * @param a expression operand of double type
	 * @param b expression operand of double type
	 * @return the object of type Result
	 * @throws ArithmeticException when divided by 0
	 */
	@Override
	public Result calculate(double a, double b) {
		double result = 0.0;
		try {
			result = a / b;
		} catch (ArithmeticException e) {
			return new Result("Invalid operation");
		}
		return new Result(result);
	}
}