package com.mariiapasichna.strategies;

import com.mariiapasichna.models.Result;

/**
 * The {@code CalculationStrategy} functional interface that provides abstract
 * method for implementation the strategy pattern.
 * 
 * @author Mariia Pasichna
 *
 */
public interface CalculationStrategy {

	/**
	 * Execute operation according to strategy
	 * 
	 * @param a expression operand of double type
	 * @param b expression operand of double type
	 * @return the object of type Result
	 */
	Result calculate(double a, double b);
}